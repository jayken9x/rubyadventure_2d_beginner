﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LaserEnemy {
public class Enemy : MonoBehaviour
{
    private float timeBtwAttack;
    public float startTimeBtwAttack;
    public Transform attackPos;
    public float attackRange;
    public LayerMask whatIsPlayer;
    public LineRenderer lineOfSight;

    void Start()
    {
        // // Dòng này để raycast không bị mất bởi bản thân
        // Physics2D.queriesStartInColliders = false;
        lineOfSight.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, distance);
        // if (hitInfo.collider != null) {
        //     // số 1 ở đầu biểu thị, đây là điểm END của Line, 0 là điểm bắt đầu
        //     lineOfSight.SetPosition(1, hitInfo.point);
        //     lineOfSight.colorGradient = redColor;

        //     if (hitInfo.collider.CompareTag("Player")) {
        //         hitInfo.collider.GetComponent<RubyController>().ChangeHealth(-1);
        //     }

        // } else {
        //     lineOfSight.SetPosition(1, transform.position + transform.up * distance);
        //     lineOfSight.colorGradient = greenColor;
        // }
    
        // lineOfSight.SetPosition(0, transform.position);

        if (timeBtwAttack <= 0) {
            StartCoroutine(AttackAni());
            Collider2D[] playerToDame = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsPlayer);
            for (int i = 0; i < playerToDame.Length; i++) {
                playerToDame[i].GetComponent<RubyController>().ChangeHealth(-1);
            }
            timeBtwAttack = startTimeBtwAttack;
        } else {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

    IEnumerator AttackAni() {
        lineOfSight.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        lineOfSight.gameObject.SetActive(false);
    }
}

}
