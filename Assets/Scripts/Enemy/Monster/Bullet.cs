﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float moveSpeed = 9f;

	Rigidbody2D rb;
	RubyController target;
	Vector2 moveDirection;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		target = GameObject.FindObjectOfType<RubyController>();
		if (target) {
			moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
			rb.velocity = new Vector2 (moveDirection.x, moveDirection.y);
			Destroy(gameObject, 3f);
		}

	}

	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.name.Equals("Ruby")) {
			col.gameObject.GetComponent<RubyController>().ChangeHealth(-1);
		}

		Monster m = col.gameObject.GetComponent<Monster>();
		if (m == null) {
			Destroy(gameObject);
		}
	}

}
