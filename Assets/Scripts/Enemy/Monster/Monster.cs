﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {
    public float speed;
	public float maxDistance;
    public float stoppingDistance;
    public float retreatDistance;

	public AudioClip shootSound;
	public AudioClip dieSound;
	public GameObject botExplosion; 

	public GameObject bullet;

	public float fireRate = 1f;
	float countDownTimeBtwShots;
	Transform player;
	AudioSource audioSource;

	void Start () {
		player = GameObject.FindObjectOfType<RubyController>().transform;
		countDownTimeBtwShots = fireRate;

		audioSource = GetComponent<AudioSource>();
		audioSource.volume = FindObjectOfType<AudioManager>().GetGeneralSoundVolume();
	}
	
	// Update is called once per frame
	void Update () {
        float distance2Player = Vector2.Distance(transform.position, player.position);

        // Follows the Charactor and Retreat when to close to him
        if (distance2Player > stoppingDistance && distance2Player < maxDistance) {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        } else if (distance2Player < stoppingDistance && distance2Player > retreatDistance) {
            // Don't move ~ keep safe distance 
        } else if (distance2Player < retreatDistance) {
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        }

		if (Vector2.Distance(transform.position, player.position) <= 6f)
			CheckIfTimeToFire();
	}

	void CheckIfTimeToFire()
	{
		if (countDownTimeBtwShots <= 0) {
			Instantiate(bullet, transform.position, Quaternion.identity);
			audioSource.volume = FindObjectOfType<AudioManager>().GetGeneralSoundVolume();
			audioSource.PlayOneShot(shootSound);
			countDownTimeBtwShots = fireRate;
		} else {
			countDownTimeBtwShots -= Time.deltaTime;
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		Projectile isShooted = other.gameObject.GetComponent<Projectile>();
		if (isShooted != null) {
			GameObject e =  Instantiate(botExplosion) as GameObject;
			e.transform.position = transform.position;
    		AudioSource.PlayClipAtPoint(dieSound, transform.position, FindObjectOfType<AudioManager>().GetGeneralSoundVolume());
			Destroy(gameObject);
		}
	}

}
