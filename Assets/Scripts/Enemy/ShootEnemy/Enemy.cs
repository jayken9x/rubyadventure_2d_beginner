﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShottingEnemy {
public class Enemy : MonoBehaviour
{
    public float speed;
    public float stoppingDistance;
    public float retreatDistance;
    public GameObject deathEffect;
	private ShottingEnemy.Weapon enemyWeapon;
	Transform player;

	public AudioClip shootSound;
	public AudioClip dieSound;
	AudioSource audioSource;

	void Start () {
		player = GameObject.FindObjectOfType<RubyController>().transform;
        enemyWeapon = GameObject.FindObjectOfType<ShottingEnemy.Weapon>();

		audioSource = GetComponent<AudioSource>();
		audioSource.volume = FindObjectOfType<AudioManager>().GetGeneralSoundVolume();
	}
	
	void FixedUpdate () {
        float distance2Player = Vector2.Distance(transform.position, player.position);

        // Follows the Charactor and Retreat when to close to him
        if (distance2Player > stoppingDistance && distance2Player < stoppingDistance + 2f) {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        } else if (distance2Player < stoppingDistance && distance2Player > retreatDistance) {
            // 
        } else if (distance2Player < retreatDistance) {
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        }

		if (distance2Player <= stoppingDistance + 2f)
			enemyWeapon.FollowTargetAndShot(shootSound);
	}

    void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.layer == LayerMask.NameToLayer("Projectile")) {
			GameObject e =  Instantiate(deathEffect) as GameObject;
			e.transform.position = transform.position;
    		AudioSource.PlayClipAtPoint(dieSound, transform.position, FindObjectOfType<AudioManager>().GetGeneralSoundVolume());
			Destroy(gameObject);
		}
	}
}

}
