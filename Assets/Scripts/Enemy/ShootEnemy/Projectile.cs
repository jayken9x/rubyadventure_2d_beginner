﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShottingEnemy {
public class Projectile : MonoBehaviour
{
    public float speed;
    public float lifetime;
    public LayerMask whatIsSolid;
    public GameObject destroyEffect;

    void Start()
    {
        Invoke("DestroyProjectile", lifetime);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, 0.1f, whatIsSolid);
        if (hitInfo.collider != null) {
            if (hitInfo.collider.CompareTag("Player")) {
                hitInfo.collider.GetComponent<RubyController>().ChangeHealth(-1);
            }
            DestroyProjectile();
        }

        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    void DestroyProjectile() {
        Instantiate(destroyEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

}