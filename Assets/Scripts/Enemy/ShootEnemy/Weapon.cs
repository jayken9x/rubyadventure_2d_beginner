﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShottingEnemy {

public class Weapon : MonoBehaviour
{
    public float offset;
    public GameObject projectile;
    public Transform target;
    public Transform shotPoint;
    private float timeBtwShots;
    public float startTimeBtwShots;

    void Start()
    {
        target = GameObject.FindObjectOfType<RubyController>().transform;
    }

    void Update()
    {

    }

    public void FollowTargetAndShot(AudioClip shootSound) {
        Vector3 difference = (target.position + Vector3.up * 0.2f) - transform.position;
        float angleToPlayer = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        // transform.rotation = Quaternion.Euler(0f, 0f, angleToPlayer + offset);

        if (timeBtwShots <= 0)
        {
            Instantiate(projectile, shotPoint.position, Quaternion.Euler(0f, 0f, angleToPlayer + offset));
            AudioSource.PlayClipAtPoint(shootSound, transform.position, FindObjectOfType<AudioManager>().GetGeneralSoundVolume());
            timeBtwShots = startTimeBtwShots;
        }
        else {
            timeBtwShots -= Time.deltaTime;
        }
    }
}

}
