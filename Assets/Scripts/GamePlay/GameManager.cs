﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour
{
    public GameObject PausePanel;
    public GameObject gameOverDialog;
    public GameObject winDialog;

    public Slider BgMusicSlider;
    public Slider GeneralSoundSlider;

    AudioManager audioManager;

    void Awake() {
        audioManager = AudioManager.Instance;
        BgMusicSlider.value = audioManager.GetBgMusicVolume();
        GeneralSoundSlider.value = audioManager.GetGeneralSoundVolume();
    }

    void Start() {
        PausePanel.SetActive(false);
        gameOverDialog.SetActive(false);
        winDialog.SetActive(false);
        AudioListener.pause = false;
    }

    public void PauseGame() {
        AudioListener.pause = true;
        Time.timeScale = 0;
        PausePanel.SetActive(true);
    }

    public void ContinueGame() {
        Time.timeScale = 1;
        AudioListener.pause = false;
        PausePanel.SetActive(false);
    }

    public void SetBgMusicVolume(Slider x) {
        audioManager.SetBgMusicVolume(x.value);
    }

    public void SetGeneralSoundVolume(Slider x) {
        audioManager.SetGeneralSoundVolume(x.value);
    }

    public void GameOver() {
        // AudioListener.pause = true;
        // Time.timeScale = 0;
        // gameOverDialog.SetActive(true);
        StartCoroutine(WaitBeforeOver(1f));
    }
    
    private IEnumerator WaitBeforeOver(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        AudioListener.pause = true;
        Time.timeScale = 0;
        gameOverDialog.SetActive(true);
    }

    public IEnumerator WinGame() {
        yield return new WaitForSeconds(1f);
        AudioListener.pause = true;
        Time.timeScale = 0;
        winDialog.SetActive(true);
        SaveLevelComplete();
    }

    private Save CreateSaveGameObject() {
        Save save = new Save();
        save.currentCompletedLevel = SceneManager.GetActiveScene().buildIndex - 1;
        return save;
    }

    public void SaveLevelComplete() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        int passedLevel = 0;

        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save saveOld = (Save)bf.Deserialize(file);
            passedLevel = saveOld.currentCompletedLevel;
            file.Close();
        }

        Save save =  CreateSaveGameObject();
        
        save.currentCompletedLevel = save.currentCompletedLevel > passedLevel ? save.currentCompletedLevel : passedLevel;

        file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);

        file.Close(); 
    }

    public void MoveNextLevel() {
        int nextScene = SceneManager.GetActiveScene().buildIndex + 1;
        nextScene = nextScene > 5 ? 2 : nextScene;
        SceneManager.LoadScene(nextScene);
    }

    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ExitToHome() {
        AudioListener.pause = false;
        SceneManager.LoadScene("Home");
    }

    public void GotoLevelSelecter() {
        AudioListener.pause = false;
        SceneManager.LoadScene("Level Selector");
    }
}
