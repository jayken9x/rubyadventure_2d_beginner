﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class HomeScreen : MonoBehaviour
{
    public GameObject settingsDialog;
    public Slider BgMusicSlider;
    public Slider GeneralSoundSlider;

    void Awake () {
        AudioManager audioManager = AudioManager.Instance;
        BgMusicSlider.value = audioManager.GetBgMusicVolume();
        GeneralSoundSlider.value = audioManager.GetGeneralSoundVolume();
    }

    void Start() {
        settingsDialog.SetActive(false);
    }

    public void StartGame() {
        SceneManager.LoadScene("Level Selector");
    }

    public void ShowSettingsDialog() {
        settingsDialog.SetActive(true);
    }

    public void OffSettingsDialog() {
        settingsDialog.SetActive(false);
    }

    public void SetBgMusicVolume(Slider x) {
        GameObject.FindObjectOfType<AudioManager>().SetBgMusicVolume(x.value);
    }

    public void SetGeneralSoundVolume(Slider x) {
        GameObject.FindObjectOfType<AudioManager>().SetGeneralSoundVolume(x.value);
    }

    public void QuitGame() {
        Application.Quit();
    }

}
