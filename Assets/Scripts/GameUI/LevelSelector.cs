﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class LevelSelector : MonoBehaviour
{
    public GameObject levelHolder;
    public GameObject levelIcon;
    public GameObject thisCanvas;
    public int numberOfLevel = 25;
    public int paddingLeftAndRight = 0;
    public Vector2 iconSpacing;

    private Rect panelDimensions;
    private Rect iconDimensions;
    private int amountPerPage;
    private int currentLevelCount = 0;
    private int passedLevel;

    void Awake() {
        LoadGameData();
        AudioManager audioManager = AudioManager.Instance;
    }

    void Start()
    {
        panelDimensions = levelHolder.GetComponent<RectTransform>().rect;
        iconDimensions = levelIcon.GetComponent<RectTransform>().rect;

        int maxInARow = Mathf.FloorToInt((panelDimensions.width + iconSpacing.x - paddingLeftAndRight * 2) / (iconDimensions.width + iconSpacing.x));
        int maxInACol = Mathf.FloorToInt((panelDimensions.height + iconSpacing.y) / (iconDimensions.height + iconSpacing.y));
        amountPerPage = maxInARow * maxInACol;
        int totalPages = Mathf.CeilToInt((float)numberOfLevel / amountPerPage);

        LoadPanels(totalPages);
    }

    void LoadPanels(int numberOfPanel)
    {
        GameObject panelClone = Instantiate(levelHolder) as GameObject;
        PageSwiper swiper = levelHolder.AddComponent<PageSwiper>();
        swiper.totalPages = numberOfPanel;

        for (int i = 1; i <= numberOfPanel; i++)
        {
            GameObject panel = Instantiate(panelClone) as GameObject;
            panel.transform.SetParent(thisCanvas.transform, false);
            panel.transform.SetParent(levelHolder.transform);
            panel.name = "Page-" + i;
            panel.GetComponent<RectTransform>().localPosition = new Vector2(panelDimensions.width * (i - 1), 0);
            SetupGrid(panel);
            int numberOfIcons = i == numberOfPanel ? numberOfLevel - currentLevelCount : amountPerPage;
            LoadIcons(numberOfIcons, panel);
        }

        Destroy(panelClone);
    }

    void SetupGrid(GameObject panel)
    {
        GridLayoutGroup grid = panel.AddComponent<GridLayoutGroup>();
        grid.cellSize = new Vector2(iconDimensions.width, iconDimensions.height);
        grid.childAlignment = TextAnchor.MiddleCenter;
        grid.padding.left = paddingLeftAndRight;
        grid.padding.right = paddingLeftAndRight;
        grid.spacing = iconSpacing;
    }

    void LoadIcons(int numOfIcons, GameObject parentObject)
    {
        for (int i = 1; i <= numOfIcons; i++)
        {
            currentLevelCount++;
            GameObject icon = Instantiate(levelIcon) as GameObject;
            icon.transform.SetParent(thisCanvas.transform, false);
            icon.transform.SetParent(parentObject.transform);
            icon.name = "Level " + i;

            Button btn = icon.GetComponent<Button>();

            // cái này chỉ để lặp lại scene :v vì ko muốn tạo hẳn 30 scene khác nhau và add từng cái vào build setting, nên lặp lại 4 cái :v
            int temp = (currentLevelCount)%(SceneManager.sceneCountInBuildSettings - 2);
            if (temp == 0) temp = 4;
            int orderOfScene = temp + 1;

            if (currentLevelCount < passedLevel + 1)
            {
                btn.onClick.AddListener(() => LoadLevel(orderOfScene));
                icon.GetComponentInChildren<Text>().text = "" + currentLevelCount;
                icon.GetComponent<LevelState>().CompleteLevel();
            }
            else if (currentLevelCount == passedLevel + 1)
            {
                btn.onClick.AddListener(() => LoadLevel(orderOfScene));
                icon.GetComponentInChildren<Text>().text = "" + currentLevelCount;
            }
            else {
                icon.GetComponentInChildren<Text>().text = "?";
                icon.GetComponent<LevelState>().NotUnlock();
            }
        }
    }

    void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void ExitToHome()
    {
        SceneManager.LoadScene("Home");
    }

    public void LoadGameData()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            passedLevel = save.currentCompletedLevel;
        }
        else
        {
            Debug.Log("No game saved!");
        }
    }
}
