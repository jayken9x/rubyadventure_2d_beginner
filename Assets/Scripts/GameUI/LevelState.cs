﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelState : MonoBehaviour
{
    public Sprite LockImage;
    
    bool isComplete = false;

    void Start() {
        Image hello = this.transform.GetChild(1).GetComponent<Image>();
        if (!isComplete)
            hello.enabled = false;
    }

    public void CompleteLevel() {
        isComplete = true;
    }

    public void NotUnlock()
    {
        this.GetComponent<Image>().sprite = LockImage;
    }
}
