﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileInput : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public RectTransform BackGround;
    public RectTransform Knob;
    public Vector2 InputDirection = Vector2.zero;

    public void OnBeginDrag(PointerEventData ped) {}

    public void OnDrag(PointerEventData ped) 
    {
        /*
            Lấy tọa độ Pointer (ped.positon - BG.position) là được rồi tại sao phải chia cho (Bg.size - Knob.size) / 2
            - Vì tọa độ Pointer lớn nên khi normalized InputDirection về Vector đơn vị sẽ luôn đạt giá trị lớn nhất (nằm trên đường tròn gốc 0, R = 1)
            - Nhưng đôi khi chúng ta chỉ muốn di chuyển một đoạn nhỏ (InputDirection phải nằm tròn hình tròn (< R), not trên đường tròn)
            - Nên chúng ta sẽ coi viền của Joystick như viền của đường tròn => sẽ chia cho (Bg.size - Knob.size) / 2
        */

        InputDirection = new Vector2(
            (ped.position.x - BackGround.position.x) / ((BackGround.rect.size.x - Knob.rect.size.x) / 2),
            (ped.position.y - BackGround.position.y) / ((BackGround.rect.size.x - Knob.rect.size.x) / 2)
        );

        InputDirection = (InputDirection.magnitude > 1.0f) ? InputDirection.normalized : InputDirection;

        // Chỉ cho phép Knob nằm trong Background
        Knob.transform.position = new Vector2(
            BackGround.position.x + (InputDirection.x * ((BackGround.rect.size.x - Knob.rect.size.x) / 2)),
            BackGround.position.y + (InputDirection.y * ((BackGround.rect.size.y - Knob.rect.size.y) / 2))
        );
    }

    public void OnEndDrag(PointerEventData ped) 
    {
        InputDirection = Vector2.zero;
        Knob.transform.position = BackGround.position;
    }

    public void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public void OnPointerUp(PointerEventData ped)
    {
        OnEndDrag(ped);
    }
}
