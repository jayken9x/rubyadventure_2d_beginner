﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get {
            if (_instance == null)
            {
                GameObject container = new GameObject();
                container.name = "Audio Manager";

                AudioSource audioSource = container.AddComponent<AudioSource>();
                audioSource.clip = Resources.Load<AudioClip>("BgMusic");
                audioSource.loop = true;
                audioSource.Play();

                _instance = container.AddComponent<AudioManager>();
            } 
            
            return _instance; 
        }
    }

    private float generalSound;

    void Awake()
    {
        if (_instance != null) {
            Destroy(this);
        }
        else
        {
            LoadMusicData();
        }

        DontDestroyOnLoad(this.gameObject);
    }

    void LoadMusicData()
    {
        if (!PlayerPrefs.HasKey("bgMusic")) {
            SetBgMusicVolume(1f);
        } else {
            gameObject.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("bgMusic");
        } 

        if (!PlayerPrefs.HasKey("generalSound")) {
            SetGeneralSoundVolume(1f);
        } else {
            generalSound = PlayerPrefs.GetFloat("generalSound");
        } 
    }

    public void SetBgMusicVolume(float volume) {
        gameObject.GetComponent<AudioSource>().volume = volume;
        PlayerPrefs.SetFloat("bgMusic", volume);
        PlayerPrefs.Save();
    }

    public float GetBgMusicVolume() {
        return PlayerPrefs.GetFloat("bgMusic");
    }

    public void SetGeneralSoundVolume(float volume) {
        generalSound = volume;
        PlayerPrefs.SetFloat("generalSound", volume);
        PlayerPrefs.Save();
    }

    public float GetGeneralSoundVolume() {
        return generalSound;
    }

}
