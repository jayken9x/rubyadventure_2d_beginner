﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubyController : MonoBehaviour
{
    [Header("Charactor Attribute")]
    public float speed = 3.0f;
    public int maxHealth = 5;
    public float timeInvincible = 1.0f;

    [Header("Reference")]
    public GameObject projectilePrefab;
    public AudioClip throwSound;
    public AudioClip hitSound;
    public GameObject joystickInput;
    public GameObject touchToShoot;
    public GameObject rubyDeathEffect; 


    [Header("Platform")]
    public bool PC = false;

    int currentHealth;
    bool isInvincible;
    float invincibleTimer;

    Rigidbody2D rigidbody2d;
    float horizontal;
    float vertical;

    Animator animator;
    Vector2 lookDirection = new Vector2(1, 0);
    AudioSource audioSource;
    MobileInput joystick;
    MobileShoot shoot;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        joystick = joystickInput.GetComponent<MobileInput>();
        shoot = touchToShoot.GetComponent<MobileShoot>();

        currentHealth = maxHealth;

        audioSource = GetComponent<AudioSource>();
        audioSource.volume = FindObjectOfType<AudioManager>().GetGeneralSoundVolume();
    }

    // Update is called once per frame
    void Update()
    {
        if (!PC)
        {
            horizontal = joystick.InputDirection.x;
            vertical = joystick.InputDirection.y;

            if (shoot.isTouch) {
                Launch();
                shoot.isTouch = false;
            }
        } else
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");

            if (Input.GetKeyDown(KeyCode.C)) Launch();
            if (Input.GetKeyDown(KeyCode.X)) TalkWithNPC();
        }

        Vector2 move = new Vector2(horizontal, vertical);

        if (!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))
        {
            lookDirection.Set(move.x, move.y);
            lookDirection.Normalize();
        }

        animator.SetFloat("Look X", lookDirection.x);
        animator.SetFloat("Look Y", lookDirection.y);
        animator.SetFloat("Speed", move.magnitude);

        if (isInvincible)
        {
            invincibleTimer -= Time.deltaTime;
            if (invincibleTimer < 0)
            {
                isInvincible = false;
            }
        }
    }

    void Launch()
    {
        GameObject projectileObj = Instantiate(projectilePrefab, rigidbody2d.position + Vector2.up * 0.5f, Quaternion.identity) as GameObject;
        Projectile projectile = projectileObj.GetComponent<Projectile>();

        float angle = Vector2.SignedAngle(lookDirection, new Vector2(1, 0));
        if (angle >= -45 && angle <= 45) lookDirection = new Vector2(1, 0);
        else if (angle > 45 && angle < 135) lookDirection = new Vector2(0, -1);
        else if (angle > -135 && angle < -45) lookDirection = new Vector2(0, 1);
        else lookDirection = new Vector2(-1, 0);

        projectile.Launch(lookDirection, 400); // fore is newtorn

        animator.SetTrigger("Launch");
        PlaySound(throwSound);
    }

    void FixedUpdate()
    {
        Vector2 position = rigidbody2d.position;
        position.x = position.x + speed * horizontal * Time.deltaTime;
        position.y = position.y + speed * vertical * Time.deltaTime;

        rigidbody2d.MovePosition(position);
    }

    public int health { get { return currentHealth; } }

    public void ChangeHealth(int amount)
    {
        if (amount < 0)
        {
            if (isInvincible)
                return;

            animator.SetTrigger("Hit");
            isInvincible = true;
            invincibleTimer = timeInvincible;

            PlaySound(hitSound);
        }

        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);

        if (currentHealth == 0) {
            GameObject e =  Instantiate(rubyDeathEffect) as GameObject;
			e.transform.position = transform.position;
            AudioSource.PlayClipAtPoint(hitSound, transform.position, audioSource.volume);
            GameOver();
            gameObject.SetActive(false);

        }

        HealthBar.instance.SetValue(currentHealth / (float)maxHealth);
    }

    void GameOver() {
        GameObject.FindObjectOfType<GameManager>().GameOver();
    }

    public void PlaySound(AudioClip clip) {
        audioSource.volume = FindObjectOfType<AudioManager>().GetGeneralSoundVolume();
        audioSource.PlayOneShot(clip);
    }

    void TalkWithNPC()
    {
        RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("NPC"));
        if (hit.collider != null)
        {
            NonPlayerCharacter character = hit.collider.GetComponent<NonPlayerCharacter>();
            if (character != null)
            {
                character.DisplayDialog();
            }
        }
    }
}
