﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    List<GameObject> enemies = new List<GameObject>();
    GameManager levelSceneManager;
    // Start is called before the first frame update
    void Start()
    {
        levelSceneManager = GameObject.FindObjectOfType<GameManager>();
        foreach (Transform child in transform)
        {
            if (!enemies.Contains(child.gameObject))
            {
                enemies.Add(child.gameObject);
            }
        }

    }

    // Update is called once per frame
    void Update() {
        if (enemies.Count == 0) {
            StartCoroutine(levelSceneManager.WinGame());
        }
    }

    public void fixedABot(GameObject bot)
    {
        if (enemies.Contains(bot)) {
            enemies.Remove(bot);
        }
    }
}
