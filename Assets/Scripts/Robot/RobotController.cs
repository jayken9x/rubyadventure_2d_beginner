﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : MonoBehaviour {

    public float speed = 1.0f;
    public bool vertical;
    public float changeTime = 4.0f;
    public ParticleSystem smokeEffect;
    EnemyManager enemyManager;
    public AudioClip fixedSound;

    Rigidbody2D rigidbody2d;
    float timer;
    int direction = 1;
    bool broken = true;

    Animator animator;
    AudioSource audioSource;
    AudioManager audioManager;

    // Start is called before the first frame update
    void Start() {
        rigidbody2d = GetComponent<Rigidbody2D>();
        timer = changeTime / 2;
        animator = GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
        enemyManager = FindObjectOfType<EnemyManager>();
        audioManager = FindObjectOfType<AudioManager>();
        audioSource.volume = audioManager.GetGeneralSoundVolume();
    }

    // Update is called once per frame
    void Update() {

        if (!broken)
            return;

        timer -= Time.deltaTime;
        
        if (timer < 0) {
            direction = -direction;
            timer = changeTime;
        }

        audioSource.volume = audioManager.GetGeneralSoundVolume();
    }

    void FixedUpdate() {
        if (!broken)
            return;

        Vector2 position = rigidbody2d.position;

        if (vertical) {
            animator.SetFloat("MoveX", 0);
            animator.SetFloat("MoveY", direction);
            position.y = position.y + Time.deltaTime * speed * direction;
        } else {
            animator.SetFloat("MoveY", 0);
            animator.SetFloat("MoveX", direction);
            position.x = position.x + Time.deltaTime * speed * direction;
        }
            

        rigidbody2d.MovePosition(position);
    }

    public void Fix() {
        smokeEffect.Stop();
        audioSource.Stop();
        audioSource.PlayOneShot(fixedSound);
        broken = false;
        rigidbody2d.simulated = false;

        animator.SetTrigger("Fixed");
        enemyManager.fixedABot(gameObject);
    }

    void OnCollisionEnter2D(Collision2D other) {
        RubyController player = other.gameObject.GetComponent<RubyController>();

        if (player != null) {
            player.ChangeHealth(-1);
        }
    }

}
